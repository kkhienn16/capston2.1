// Dependencies 

const mongoose = require("mongoose");


// User model
const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true,"Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true,"Mobile Number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	cartItem: [
			{
				products: [
					{
						productId:{
							type: String,
							required: true
						},
						productModel: {
							type: String,
							required: true
						},
						price: {
							type: Number,
							required: true
						},
						quantity: {
							type: Number,
							required: true
						}
						
					}
				],

				subTotal: {
					type: Number,
					required:[true,"subTotal is required"]
				},
				createdAt:{
					type: Date,
					default: new Date()
				}
			}
		],
	orderedProducts: [
			{
				products: [
						{
							productId:{
								type: String,
								required: [true, "Product ID is required"]
							},
							productModel: {
								type: String,
								required: [true, "Product name is required"]
							},
							price: {
								type: Number,
								required: true
							},
							quantity: {
								type: Number,
								required: [true, "Quantity is required"]
							}
						}
					],

				subTotal: {
					type: Number,
					required:[true,"Total amount is required"]
				},
				purchasedOn: {
					type: Date,
					default: new Date()
				}
			}

		],

})

// Model
module.exports = mongoose.model("User",userSchema);