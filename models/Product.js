// Dependencies
const mongoose = require("mongoose");

// Product model

const productSchema = new mongoose.Schema({

  brand: {
    type: String,
    required: [true, "Name is required"]
  },
  model:{
    type: String,
    required: [true, "model is required"]
  },
  size: {
    type: String,
    required: [true, "Size is required"]
  },
 
  description: {
    type: String,
    required: [true,"Description is required"]
  },
  price: {
    type: Number,
    required: [true, "Price is required"]
  },
  stock: {
    type: Number,
    required: [true, "Stock is required"]
  },
  isActive: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    default: new Date()
  },
  userOrders: [
      {
        userId: {
          type: String,
          required: [true, "UserId is required"]
        },

      }
    ],
  imageUrl: {
    type: String // This field will store the image URL
  }

})

// Model
module.exports = mongoose.model("Product",productSchema);