//[SECTION] Dependencies and Modules
	const express = require('express');
	const userController = require("../controllers/user");
	const auth = require("../auth") 

	const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
	const router = express.Router();




//[SECTION] Route for user registration
router.post("/register",(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//[SECTION] Route for user authentication
router.post("/login", userController.loginUser);


// Route for retrieving user details
	//router.post("/details", verify, userController.getProfile);

	//Refactor
	router.get("/details", verify, userController.getProfile);

//Update Admin route
	router.put('/:userId/updateAdmin', verify, verifyAdmin, userController.updateUserAsAdmin);

//Update Admin route
	router.put('/:userId/updateUser', verify, verifyAdmin, userController.updateAdminAsUser);	



//ChatGPT Generated Codes

//[SECTION] Reset Password
	router.put('/reset-password', verify, userController.resetPassword);

//[SECTION] Update Profile	
	router.put('/profile', verify, userController.updateProfile);

//Route to create order
router.post("/checkout",verify ,userController.order);

// Retrieve authenticated users orders
router.get('/orders', verify, userController.getUserOrders);

// Retrieve all orders (admin only)
router.get('/allOrders', verify, verifyAdmin, userController.getAllOrders);

// Retrieve all users list (admin only)
router.get('/allUsers', verify, verifyAdmin, userController.getRegularUsers);


// Add to cart (Authenticated User)
router.post('/cart/:productId',verify, userController.addToCart);

// update cart item quantity (authenticated User)
router.put('/cart/update/:productId', verify, userController.updateCartItemQuantity);

// Remove items from cart (authenticated User)
router.delete('/cart/delete/:productId', verify, userController.deleteItem);

// Checkout items from cart (Admin only)
router.delete('/cart/checkout/:productId', verify, userController.checkoutAndRemoveItem);


// calculating the sum of all items in cart
router.get('/cart',verify , userController.total)

//[SECTION] Export Route System
	module.exports = router; 
















