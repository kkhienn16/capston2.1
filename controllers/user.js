//[SECTION] Dependencies and Modules
	const User = require("../models/User");
	const Product = require("../models/Product");
	const bcrypt = require('bcrypt');
	const auth = require("../auth");


// User Registration

module.exports.registerUser = async (reqbody) => {
  try {

    // Check if the email is already in use
    const existingUser = await User.findOne({ email: reqbody.email });
    
    if (existingUser) {
       return res.status(404).send({ message: "Email is already in use" });
    }

    // If the email is not in use, proceed with registration
    let newUser = new User({
      firstName: reqbody.firstName,
      lastName: reqbody.lastName,
      email: reqbody.email,
      mobileNo: reqbody.mobileNo,
      password: bcrypt.hashSync(reqbody.password,10)
   	  
    });

    const user = await newUser.save();
    return true; // Return true upon successful user registration

  } catch (error) {
    console.error('Error registering user:', error);
    return false; // Return false if an error occurs during user registration
  }
};




//[SECTION] User authentication

	module.exports.loginUser = (req, res) => {
		User.findOne({ email : req.body.email} ).then(result => {

			console.log(result);

			// User does not exist
			if(result == null){

				return res.send(false);

			// User exists
			} else {
				
				const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
				// If the passwords match/result of the above code is true
				if (isPasswordCorrect) {
	
					return res.send({ access : auth.createAccessToken(result) })

				// Passwords do not match
				} else {

					return res.send(false);
				}
			}
		})
		.catch(err => res.send(err))
	};




//[SECTION] Retrieve user details

	module.exports.getProfile = (req, res) => {

		return User.findById(req.user.id)
		.then(result => {
			result.password = "";
			return res.send(result);
		})
		.catch(err => res.send(err))
	};





	//ChatGPT Generated

	//[SECTION] Reset Password

	module.exports.resetPassword = async (req, res) => {
		try {

		//console.log(req.user)
		//console.log(req.body)

		  const { newPassword } = req.body;
		  const { id } = req.user; // Extracting user ID from the authorization header
	  
		  // Hashing the new password
		  const hashedPassword = await bcrypt.hash(newPassword, 10);
	  
		  // Updating the user's password in the database
		  await User.findByIdAndUpdate(id, { password: hashedPassword });
	  
		  // Sending a success response
		  res.status(200).send({ message: 'Password reset successfully' });
		} catch (error) {
		  console.error(error);
		  res.status(500).send({ message: 'Internal server error' });
		}
	};

  

	//[SECTION] Reset Profile
	module.exports.updateProfile = async (req, res) => {
		try {

			console.log(req.user);
			console.log(req.body);
			
		// Get the user ID from the authenticated token
		  const userId = req.user.id;
	  
		  // Retrieve the updated profile information from the request body
		  const { firstName, lastName, mobileNo } = req.body;
	  
		  // Update the user's profile in the database
		  const updatedUser = await User.findByIdAndUpdate(
			userId,
			{ firstName, lastName, mobileNo },
			{ new: true }
		  );
	  
		  res.send(updatedUser);
		} catch (error) {
		  console.error(error);
		  res.status(500).send({ message: 'Failed to update profile' });
		}
	  }

	

//Update user as admin controller
exports.updateUserAsAdmin = (req, res) => {
	  let isAdmin = {
    isAdmin: true
  }
  return User.findByIdAndUpdate(req.params.userId, isAdmin).then((user,error) => {
    if(error){
      return res.send(false);
    } else {
      return res.send(true);
    }
  })

};

exports.updateAdminAsUser = (req, res) => {
    let isAdmin = {
    isAdmin: false
  }
  return User.findByIdAndUpdate(req.params.userId, isAdmin).then((user,error) => {
    if(error){
      return res.send(false);
    } else {
      return res.send(true);
    }
  })

};





// Create Order

module.exports.order = async (req, res) => {
    try {
      // Check if the user is an admin and deny the checkout
      if (req.user.isAdmin) {
        return res.status(403).send("Action Forbidden"); 
      }

      const { productId, quantity } = req.body; // Extract productId and quantity from request body

      // Fetch the product from the database to get its price
      const product = await Product.findById(productId);

      if (!product) {
        return res.status(404).send({ message: "Product not found" });
      }

      const model = product.model;
      const price = product.price;
      const totalPrice = product.price * quantity; // Calculate the total price based on quantity and product's price

      // Check if the ordered quantity is available in stock
      if (quantity > product.stock) {
        return res.status(400).send({ message: "Insufficient stock" });
      }

      // Update the user's orderedProducts array with productId, quantity, and totalAmount
      const updatedUser = await User.findByIdAndUpdate(
        req.user.id,
        { $push: 
          { 
            orderedProducts: 
              { products: { productId, productModel: model , price: price , quantity,},
              subTotal: totalPrice } } },
        { new: true }
      );

      if (!updatedUser) {
        return res.status(500).send({ message: "Error updating user" });
      }

      // Update the product's userOrders array with userId
      const updatedProduct = await Product.findByIdAndUpdate(
        productId,
        { $push: { userOrders: { userId: req.user.id } }, $inc: { stock: - quantity } },
        { new: true }
      );

      if (!updatedProduct) {
        return res.status(500).send({ message: "Error updating product" });
      }

      return res.send({ message: "Purchase Successfully" });
    }

    catch (error) {
      console.error('Error during order:', error);
      res.status(500).json({ message: 'An error occurred during order' });
    }
};



// Retrieve authenticated users orders

module.exports.getUserOrders = (req,res) => {
  
  return User.findById(req.user.id).then(result => {
      
      return res.send({ Orders: result.orderedProducts })
  })

  .catch(error => err)

}

// Retrieve all users orders (admin only)

module.exports.getAllOrders = (req, res) => {
  User.find({})
    .then(users => {
      const allOrderedProducts = [];
      
      users.forEach(user => {
        allOrderedProducts.push(...user.orderedProducts);
      });
      
      return res.send(allOrderedProducts);
    })
    .catch(error => err)
};


module.exports.getRegularUsers = (req, res) => {
  // Find regular users with isAdmin set to false
  User.find({ isAdmin: { $in: [false, true] } })
    .select('_id firstName lastName email isAdmin') // Select the fields to retrieve
    .then(regularUsers => {
      return res.json(regularUsers);
    })
    .catch(error => {
      return res.status(500).json({ error: 'An error occurred while fetching regular users.' });
    });
};




// Add to cart

module.exports.addToCart = async (req,res) => {
  try {
    // Check if the user is an admin and deny the add to cart
    if(req.user.isAdmin) {
      return res.status(403).send("Action Forbidden");
    }

    const { quantity } = req.body; //Extract quantity from req body
    const productId = req.params.productId;


    // Fetch the product from the database to get its price
    const product = await Product.findById(productId);

    if (!product) {
      return res.status(404).send ({ message: "Product not found"});
    }

    // Calculate the total price based on quantity and product's price
    const subTotal = product.price * quantity;
    const model = product.model;
    const price = product.price;

    // Check if the ordered quantity is available in stock
    if (quantity > product.stock) {
      return res.status(400).send({ message: "Insufficient stock" });
    }

     // Update the user's cartItem array with productId, quantity, and totalAmount
    const updatedUser = await User.findByIdAndUpdate(
      req.user.id,
      { $push: { 
        cartItem:{ 
          products:
          {productId, 
          quantity,
          price: price,
          productModel: model,},
          subTotal: subTotal
           } } },
      { new: true }
    );

     if (!updatedUser) {
      return res.status(500).send({ message: "Error updating user" });
    }

    return res.send({ message: "Add to cart Successfully" });

  } catch (error) {
    res.status(500).json({ error: 'An error occurred while Adding item to cart' });
  }

  
}


// update cart's item quantity

module.exports.updateCartItemQuantity = async (req, res) => {
  try {
    const userId = req.user.id; // Extract user ID from the authenticated user's token
    const productId = req.params.productId; // Extract the product ID from the route parameters
    const newQuantity = req.body.quantity; // Extract the new quantity from the request body

    // Find the user document based on the extracted user ID
    const user = await User.findById(userId);

    // Check if the user exists
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Find the cart item within the user's cart based on the extracted product ID
    const cartItemToEdit = user.cartItem.find(item => item.productId === productId);

    // Check if the cart item exists
    if (!cartItemToEdit) {
      return res.status(404).json({ message: 'Cart item not found' });
    }

    // Find the corresponding product in the database based on the product ID
    const product = await Product.findById(productId);

    // Check if the product exists
    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }

    // Check if the new quantity is zero
    if (newQuantity === 0) {
      return res.status(400).json({ message: 'Invalid quantity' });
    }

    // Check if the new quantity exceeds the available stock for the product
    if (newQuantity > product.stock) {
      return res.status(400).json({ message: 'Insufficient stock' });
    }

    // Update the cart item's quantity and subTotal based on the new quantity and product price
    cartItemToEdit.quantity = newQuantity;
    cartItemToEdit.subTotal = newQuantity * product.price;

    // Save the updated user document with the modified cart item
    await user.save();

    // Respond with a success message and the updated cart items
    res.status(200).json({ message: 'Cart item quantity updated successfully', cart: user.cartItem });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};



// Sum of all item in cart
module.exports.total = async (req, res) => {
  try {
    const userId = req.user.id;

    // Find the user in the database using their ID
    const user = await User.findById(userId);

    // Check if the user exists
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Calculate the total price of all items in the cart using the reduce method
    const Total = user.cartItem.reduce((total, cartItem) => total + cartItem.subTotal, 0);

    // Send the calculated cart total as a response
    res.status(200).json({ Total });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Oops! Something went wrong on our side.' });
  }
};


// Remove Item from cart

module.exports.deleteItem = (req, res) => {
  const userId = req.user.id; 
  const productId = req.params.productId;
  console.log(`Removing productId: ${productId} from userId: ${userId}`);

  User.findByIdAndUpdate(
    userId,
     { $pull: { cartItem: { 'products.productId': productId } } },
    { new: true }
  )
    .then(updatedUser => {
      if (!updatedUser) {
         console.log("User not found.");
        return res.status(404).send(false);
      }
      console.log("Item removed successfully.");
      return res.send(true);
    })
    .catch(error => {
      console.error(error);
      return res.status(500).send(false);
    });
};

// checkout Item from the Cart

module.exports.checkoutAndRemoveItem = async (req, res) => {
  const userId = req.user.id;
  const productId = req.params.productId;
  const quantity = req.body.quantity || 1; // Default quantity to 1 if not provided

  try {
    // Find the product by its ID
    const product = await Product.findById(productId);

    if (!product) {
      console.log("Product not found.");
      return res.status(404).send(false);
    }

    const model = product.model;
    const price = product.price;
    const totalPrice = price * quantity;

    console.log(`Checking out ${quantity} of productId: ${productId} from userId: ${userId}`);

    // Find the user and update the cart
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      {
        $pull: { cartItem: { 'products.productId': productId } },
        $addToSet: {
          orderedProducts: {
            products: {
              productId,
              productModel: model,
              price,
              quantity,
            },
            subTotal: totalPrice,
          },
        },
      },
      { new: true }
    );

    if (!updatedUser) {
      console.log("User not found.");
      return res.status(404).send(false);
    }

    console.log("Item checked out and removed successfully.");
    return res.send(true);
  } catch (error) {
    console.error(error);
    return res.status(500).send(false);
  }
};
