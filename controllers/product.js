// controller
const Product = require("../models/Product");
const User = require("../models/User");
const multer = require('multer');
const path = require('path');
const fs = require('fs');

// Configure Multer for handling image uploads
const storage = multer.diskStorage({
  destination: 'public/uploads/', // Specify the destination folder for uploaded images
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    cb(null, file.fieldname + '-' + uniqueSuffix + path.extname(file.originalname));
  },
});
const upload = multer({ storage: storage });

// Add the product with image upload and store the image URL in the database
module.exports.addProduct = (req, res) => {
  upload.single('productImage')(req, res, (uploadError) => {
    if (uploadError) {
      return res.send('Error uploading image');
    }

    const imageUrl = '/uploads/' + req.file.filename; // Define the image URL

    // Create a new product object with data from the request
    const newProduct = new Product({
      brand: req.body.brand,
      model: req.body.model,
      size: req.body.size,
      description: req.body.description,
      price: req.body.price,
      stock: req.body.stock,
      imageUrl: imageUrl, // Store the image URL in the database
    });

    // Save the created object to our database
    newProduct
      .save()
      .then((product) => {
        // Product creation successful
        return res.send(true);
      })
      .catch((err) => res.send(err));
  });
};


// Get active products
module.exports.getAllActiveProducts = (req,res) => {
	return Product.find({ isActive :true }).then(result => {
		  return res.send(result);
	})
  .catch(err => res.send(err))
}


// Get all products
module.exports.getAllProducts = (req, res) => {

  return Product.find({}).then(result => {
      return res.send(result);
  
  })
  .catch(err => res.send(err))
};

// Retrieve a single product
module.exports.getProduct = (req, res) => {
  Product.findById(req.params.productId)
    .then(product => {
      if (!product) {
        return res.status(404).json({ message: 'Product not found' });
      }

      // Format the product data
      const formattedProduct = {
        brand: product.brand,
        model: product.model,
        description: product.description,
        size: product.size,
        price: product.price,
        stock: product.stock
      };

      // Return the formatted product
      res.json(formattedProduct);
    })
    .catch(error => {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    });
};
// updating a product information

module.exports.updateProduct = (req,res) => {

	let updatedProduct = {
		brand: req.body.brand,
		model: req.body.model,
    description: req.body.description,
		size: req.body.size,
		stock: req.body.stock,
		price: req.body.price
		
	};

	return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) =>{
		if(error){
			return res.send(false);
		}else {
			return res.send(true);
		}
	})
  .catch(err => res.send(err))
};

// Archive 
module.exports.productArchive = (req,res) => {
	let availability = {
		isActive: false
	}
	return Product.findByIdAndUpdate(req.params.productId, availability).then((product,error) => {
		if(error){
			return res.send(false);
		} else {
			return res.send(true);
		}
	})

} 

//activate
module.exports.productActivate = (req,res) => {
  let availability = {
    isActive: true
  }
  return Product.findByIdAndUpdate(req.params.productId, availability).then((product,error) => {
    if(error){
      return res.send(false);
    } else {
      return res.send(true);
    }
  })

} 

// filter products by price 
module.exports.filterProductByPrice = async (req, res) => {
  try {
    const { minPrice, maxPrice } = req.body;

    // Query products within the given price range
    const products = await Product.find({
      price: { $gte: minPrice, $lte: maxPrice }
    });

    // Create an array to store the formatted product data
    const formattedProducts = products.map(product => ({
      brand: product.brand,
      model: product.model,
      description: product.description,
      size: product.size,
      price: product.price,
      stock: product.stock
    }));

    res.json(formattedProducts);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


// Filter products by name
module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const products = await Product.find({
      brand: { $regex: productName, $options: 'i' }
    });

    if (products.length === 0) {
      return res.status(404).json({ message: 'Results not found' });
    }

    // Create an array to store the formatted product data
    const formattedProducts = products.map(product => ({
      brand: product.brand,
      model: product.model,
      description: product.description,
      size: product.size,
      price: product.price,
      stock: product.stock
    }));

    res.json(formattedProducts);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};



